alias ls-pci='mhwd --pci --listall'
alias ls-usb='mhwd --usb --listall'
alias ls-audio='inxi -Aa'

alias lsh-pci='mhwd --pci --listhardware'
alias lsh-usb='mhwd --usb --listhardware'

