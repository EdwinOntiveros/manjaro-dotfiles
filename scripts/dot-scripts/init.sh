# Example aliases
alias bashrc="$EDITOR ~/.bashrc"
alias zshrc="$EDITOR  ~/.zshrc"
alias ohmyzshrc="$EDITOR  ~/.oh-my-zsh"

#user folders
export USER_SCRIPTS=$HOME/.scripts
export USER_SOURCES=$HOME/.sources

source "$USER_SCRIPTS/utils/file_sourcing.sh"

#source user aliases
USER_ALIASES="$USER_SCRIPTS/aliases"
if [ -d "$USER_ALIASES" ]; then
    [[ -f "${USER_ALIASES}/init.sh" && -r "${USER_ALIASES}/init.sh" ]] && {
        . ${USER_ALIASES}/init.sh
    }
fi
unset USER_ALIASES

#auto runs
USER_AUTORUNS="$USER_SCRIPTS/autorun"
if [ -d "$USER_AUTORUNS" ]; then
    [[ -f "${USER_AUTORUNS}/init.sh" && -r "${USER_AUTORUNS}/init.sh" ]] && {
        . ${USER_AUTORUNS}/init.sh
    }
fi
unset USER_AUTORUNS
