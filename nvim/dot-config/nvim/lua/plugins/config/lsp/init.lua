local lsp = require'lspconfig'

local M = {}

M.Servers = function()
    local servers = {
    [ "clangd" ] = {
            init_server = function(...)
                local args = {...}
                lsp.clangd.setup(args or {})
            end,
        },
    }
    return servers
end

M.init = function()
    local servers = M:Servers()
    if type(servers) == 'table' then
        for _, server in pairs(servers) do
            server.init_server()
        end
    end
end

return M

