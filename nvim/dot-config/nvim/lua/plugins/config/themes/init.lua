local cmd = vim.cmd

local M = {}

M._currentTheme = nil

M._themes = {
    [ "dracula" ] = {
        name = "dracula",
        repo = 'Mofiqul/dracula.nvim',
        set = function()
            cmd('colorscheme dracula')
            M._currentTheme = "dracula-nvim"
        end
    },

    [ "gruvbox" ] = {
        name = "gruvbox",
        repo = 'ellisonleao/gruvbox.nvim',
        set = function(...)
            local args = {...}
            local bg_mode = select(1, args)
            vim.opt.background = bg_mode[0] or 'dark'
            M._currentTheme = "gruvbox "..(bg_mode[0] or 'dark')
            cmd('colorscheme gruvbox')
        end
    },
}

M.Themes = function()
    return M._themes
end

M.setup = function(name, ...)
    local themes = M:Themes()
    local args = {...}
    local theme = name or 'dracula'

    if themes[theme] ~= nil then
        themes[theme].set(args)
    else
        print("Theme "..theme.." not installed")
        themes['dracula'].set() -- fallback theme
    end
end


M.activeTheme = function()
    print("Current theme :"..M._currentTheme)
end

return M

