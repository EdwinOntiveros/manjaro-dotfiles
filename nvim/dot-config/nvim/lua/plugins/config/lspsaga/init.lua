local saga = require'lspsaga'
local map  = require'utils/keymap'.map

saga.init_lsp_saga()

-- find definition of word under cursor
map('n', "<leader> sh", ":Lspsaga lsp_finder<CR>", {silent = true})

-- signature help
map('n', "<leader> sH", ":Lspsaga signature_help<CR>", {silent = true})

-- code action menu
map('n', "<leader> ..", ":Lspsaga code_action<CR>", {silent = true})

-- show hover doc
map('n', "<leader> sK", ":Lspsaga hover_doc<CR>", {silent = true})
map('n', "<leader> <up>", ":lua require'lspsaga.action'.smart_scroll_with_saga(1)<CR>", {silent = true})
map('n', "<leader> <down>", ":lua require'lspsaga.action'.smart_scroll_with_saga(-1)<CR>", {silent = true})
