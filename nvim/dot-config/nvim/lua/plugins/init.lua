local cmd = vim.cmd
local exec = vim.api.nvim_exec
local themes = require 'plugins/config/themes'.Themes()

cmd [[packadd packer.nvim]]

exec([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost lua/plugins/init.lua source <afile> | PackerCompile | PackerSync
  augroup end
]], false)

return require'packer'.startup(function(use)

    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- performance improvers
    use { "nathom/filetype.nvim" }
    use {'lewis6991/impatient.nvim'}

    -- tpope plugins
    use 'tpope/vim-repeat'
    use 'tpope/vim-surround'
    use 'tpope/vim-speeddating'
    use 'tpope/vim-unimpaired'
    use 'tpope/vim-commentary'

    -- syntax, lsp and autocompletion
    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
    use { 'neovim/nvim-lspconfig' }
    use { 'glepnir/lspsaga.nvim', requires = {'neovim/nvim-lspconfig'} }
    use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
    use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp
    use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp
    use 'L3MON4D3/LuaSnip' -- Snippets plugin

    -- themes
    use { themes['dracula'].repo }
    use { themes['gruvbox'].repo }

    -- lualine
    use {'nvim-lualine/lualine.nvim',
          requires = { 'kyazdani42/nvim-web-devicons', opt = true }}

end)

